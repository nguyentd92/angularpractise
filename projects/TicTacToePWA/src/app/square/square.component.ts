import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-square',
  template: `
    <button nbButton *ngIf="value == 'X'" status="primary">{{ value }}</button>
    <button nbButton *ngIf="value == 'O'" status="success">{{ value }}</button>
    <button nbButton *ngIf="value == null" status="basic"></button>
  `,
  styles: [
    `:host {
      width: 100%;
      height: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      font-family: sans-serif;
      font-size: 3em;
    }`,
    `
      button {
        height: 100%;
        width: 100%;
      }
    `
  ]
})
export class SquareComponent {
  @Input() value: 'X' | 'O';

  constructor() { }

}
