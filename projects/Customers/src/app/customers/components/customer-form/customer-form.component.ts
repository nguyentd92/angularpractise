import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CustomersService } from "../../services";
import { ICustomer } from "../../models/ICustomer";

@Component({
  selector: "app-customer-form",
  templateUrl: "./customer-form.component.html",
  styleUrls: ["./customer-form.component.scss"]
})
export class CustomerFormComponent implements OnInit, OnChanges {
  profileForm: FormGroup;
  @Input() id = null;
  @Output() addCustomer = new EventEmitter<ICustomer>();
  @Output() updateCustomer = new EventEmitter<ICustomer>();

  constructor(
    private fb: FormBuilder,
    private customerService: CustomersService
  ) {}

  ngOnInit() {
    this.profileForm = this.fb.group({
      fullName: ["", Validators.required],
      address: ["", [Validators.required]],
      phone: ["", [Validators.required]],
      avatar: [null]
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ("id" in changes && this.id) {
      this.customerService.getById(this.id).subscribe(e => this.profileForm.patchValue(e))
    };
  }

  get f() {
    return this.profileForm.controls;
  }

  get avatarBase64(): string {
    return this.profileForm.value.avatar;
  }

  submit() {
    const body = this.profileForm.value

    if(!this.id) {
      this.customerService
      .create(body)
      .subscribe(res => {
        this.addCustomer.emit(res)
        this.profileForm.reset()
      });
    } else {
      this.customerService.update(this.id, body).subscribe(res => this.updateCustomer.emit(res))
    }
  }

  handleUpload(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.profileForm.get("avatar").setValue(reader.result)
    };
  }

  reset() {
    this.profileForm.reset()
  }
}
