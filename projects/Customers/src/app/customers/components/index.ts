import { CustomerFormComponent } from "./customer-form/customer-form.component";
import { CustomerItemComponent } from "./customer-item/customer-item.component";

export const components = [CustomerFormComponent, CustomerItemComponent];

export * from "./customer-form/customer-form.component";
export * from "./customer-item/customer-item.component";
