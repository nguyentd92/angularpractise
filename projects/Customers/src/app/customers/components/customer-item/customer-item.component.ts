import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ICustomer } from "../../models/ICustomer";
import { CustomersService } from "../../services";
import { DEFAULT_IMAGE } from '../../resources/defaultimage';

@Component({
  selector: "app-customer-item",
  templateUrl: "./customer-item.component.html",
  styleUrls: ["./customer-item.component.scss"]
})
export class CustomerItemComponent implements OnInit {
  @Input() customer: ICustomer;
  @Output() deleteCustomer = new EventEmitter<number>();
  defaultImg = DEFAULT_IMAGE;

  constructor(private customerService: CustomersService) {}

  ngOnInit() {}

  removeCustomer() {
    const id = this.customer.id;
    this.customerService
      .delete(id)
      .subscribe(res => this.deleteCustomer.emit(id));
  }
}
