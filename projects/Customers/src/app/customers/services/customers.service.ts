import { Injectable } from "@angular/core";
import { ICustomer } from "../models/ICustomer";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

const ENDPOINT = "http://localhost:3000/customers/";

@Injectable()
export class CustomersService {
  constructor(private http: HttpClient) {}

  get(): Observable<ICustomer[]> {
    return this.http.get<ICustomer[]>(ENDPOINT);
  }

  getById(id: string): Observable<ICustomer> {
    return this.http.get<ICustomer>(ENDPOINT + id);
  }

  create(customer: ICustomer): Observable<ICustomer> {
    return this.http.post<ICustomer>(ENDPOINT, customer);
  }

  update(id: string, customer: ICustomer): Observable<ICustomer> {
    return this.http.put<ICustomer>(ENDPOINT + id, customer);
  }

  delete(id: number): Observable<ICustomer> {
    return this.http.delete<ICustomer>(ENDPOINT + id);
  }
}
