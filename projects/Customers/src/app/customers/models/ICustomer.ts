export interface ICustomer {
  id: number;
  fullName: string;
  address: string;
  phone: string;
  avatar: string;
}
