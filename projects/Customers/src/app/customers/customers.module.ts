import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import * as fromComponents from "./components";
import * as fromContainers from "./containers";
import * as fromServices from "./services";

const CUSTOMERS_ROUTES: Routes = [
  {
    path: "",
    component: fromContainers.CustomersLayoutComponent
  },
  {
    path: "**",
    redirectTo: "",
    pathMatch: "full"
  }
];

@NgModule({
  declarations: [...fromComponents.components, ...fromContainers.containers],
  imports: [
    CommonModule,
    RouterModule.forChild(CUSTOMERS_ROUTES),
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [...fromServices.services],
  exports: [RouterModule]
})
export class CustomersModule {}
