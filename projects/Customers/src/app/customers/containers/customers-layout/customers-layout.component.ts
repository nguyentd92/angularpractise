import { Component, OnInit } from "@angular/core";
import { CustomersService } from "../../services";
import { ICustomer } from "../../models/ICustomer";
import { ActivatedRoute } from "@angular/router";
import { fade } from "../../resources/animations";
import { map } from "rxjs/operators";

@Component({
  selector: "app-customers-layout",
  templateUrl: "./customers-layout.component.html",
  styleUrls: ["./customers-layout.component.scss"],
  animations: [fade]
})
export class CustomersLayoutComponent implements OnInit {
  screenHeight = window.innerHeight;
  customerList: ICustomer[] = [];
  id = null;

  constructor(
    private customerService: CustomersService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.customerService
      .get()
      .pipe(map(res => res.reverse()))
      .subscribe(res => (this.customerList = res));

    this.route.queryParams.subscribe(e => (this.id = e.selectedId));
  }

  removeCustomer(id: number) {
    this.customerList = this.customerList.filter(idx => idx.id != id);
  }

  updateOrCreateCustomer(customer: ICustomer) {
    const index = this.customerList.findIndex(ctm => ctm.id == customer.id);
    if (!isNaN(index)) {
      const { fullName, address, phone, avatar } = customer;
      const selectedCustomer = this.customerList[index] as ICustomer;
      selectedCustomer.fullName = fullName;
      selectedCustomer.address = address;
      selectedCustomer.phone = phone;
      selectedCustomer.avatar = avatar;
    } else {
      this.customerList.unshift(customer);
    }
  }
}
