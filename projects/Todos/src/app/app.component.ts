import { Component } from "@angular/core";
import { ITodo } from "./models/Todo";
import { fade } from "./resources/animations";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  animations: [fade]
})
export class AppComponent {
  title = "Todos";

  todos: ITodo[] = [
    {
      title: "Buy some chocolates",
      state: true
    },
    {
      title: "Reply to amazon",
      state: true
    },
    {
      title: "Find a job",
      state: true
    },
    {
      title: "Wash the stairs",
      state: true
    },
    {
      title: "Clean the yard",
      state: true
    },
    {
      title: "Water flowers",
      state: true
    }
  ];

  removeItem(todo) {
    this.todos = this.todos.filter(item => item !== todo);
  }
}
