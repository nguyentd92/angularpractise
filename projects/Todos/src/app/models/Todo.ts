export interface ITodo {
  title: string;
  state: boolean;
}
