import { TodoItemComponent } from "./todo-item/todo-item.component";
import { HeaderComponent } from "./header/header.component";

export const components = [TodoItemComponent, HeaderComponent];

export * from "./todo-item/todo-item.component";
export * from "./header/header.component";
