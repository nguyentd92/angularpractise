import {
  Component,
  OnInit,
  Input,
  HostBinding,
  HostListener,
  Output,
  EventEmitter
} from "@angular/core";
import { ITodo } from "../../models/Todo";

@Component({
  selector: "app-todo-item",
  templateUrl: "./todo-item.component.html",
  styleUrls: ["./todo-item.component.scss"]
})
export class TodoItemComponent implements OnInit {
  @Input() todo: ITodo;

  constructor() {}

  @Output() remove = new EventEmitter<ITodo>();

  @HostBinding("class.disabled") disabled = false;
  @HostListener("click") toggle() {
    this.disabled = this.todo.state = !this.todo.state;
  }

  ngOnInit() {
    this.disabled = this.todo.state = !this.todo.state;
  }
}
