import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { ITodo } from "../../models/Todo";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  @Output() addItem = new EventEmitter<ITodo>();

  constructor() {}

  ngOnInit() {}

  addTodo(event) {
    const todo = {
      title: event.target.value,
      state: true
    };

    event.target.value = "";

    this.addItem.emit(todo);
  }
}
