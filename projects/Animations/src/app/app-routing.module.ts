import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import * as fromContainer from "./containers";

const routes: Routes = [
  {
    path: "",
    redirectTo: "fundamental",
    pathMatch: "full"
  },
  {
    path: "fundamental",
    component: fromContainer.FundamentalComponent
  },
  {
    path: "stagger",
    component: fromContainer.StaggerComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
