import { Component, OnInit } from "@angular/core";
import {
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes
} from "@angular/animations";

const myAwesomeAnimation = trigger("myAwesomeAnimation", [
  state(
    "small",
    style({
      transform: "scale(1)"
    })
  ),
  state(
    "large",
    style({
      transform: "scale(1.2)"
    })
  ),
  transition(
    "small => large",
    animate(
      "1000ms ease-in-out",
      keyframes([
        style({
          transform: "translateY(-75%) scale(1.05)",
          offset: 0.25
        }),
        style({
          transform: "translateY(35px) scale(1.1)",
          offset: 0.5
        }),
        style({
          transform: "translateY(-75%) scale(1.15)",
          offset: 0.75
        }),
        style({
          transform: "translateY(0) scale(1.2)",
          offset: 1
        })
      ])
    )
  ),
  transition(
    "large => small",
    animate(
      "1000ms ease-in-out",
      keyframes([
        style({
          transform: "translateY(0) scale(1.15)",
          offset: 0.25
        }),
        style({
          transform: "translateY(-75%) scale(1.1)",
          offset: 0.5
        }),
        style({
          transform: "translateY(35px) scale(1.05)",
          offset: 0.75
        }),
        style({
          transform: "translateY(0) scale(1)",
          offset: 1
        })
      ])
    )
  )
]);

@Component({
  selector: "app-fundamental",
  templateUrl: "./fundamental.component.html",
  styleUrls: ["./fundamental.component.scss"],
  animations: [myAwesomeAnimation]
})
export class FundamentalComponent implements OnInit {
  state = "small";

  constructor() {}

  ngOnInit() {}

  animateMe() {
    this.state = this.state === "small" ? "large" : "small";
  }
}
