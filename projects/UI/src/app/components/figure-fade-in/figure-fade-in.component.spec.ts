import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FigureFadeInComponent } from './figure-fade-in.component';

describe('FigureFadeInComponent', () => {
  let component: FigureFadeInComponent;
  let fixture: ComponentFixture<FigureFadeInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FigureFadeInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FigureFadeInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
