import { Component, OnInit } from '@angular/core';
import { moveIn, fallIn, moveInLeft } from '../../resources/router.animation';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
  animations: [moveIn(), fallIn(), moveInLeft()],
  host: { '[@moveIn]': '' }
})
export class MembersComponent {

  authState$: Observable<firebase.User>

  constructor(public af: AuthenticationService, private router: Router) {
    this.authState$ = this.af.userData$;
  }

  logout() {
    this.af.SignOut();
    this.router.navigateByUrl('/login');
  }
}
