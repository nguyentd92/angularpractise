import { Component, OnInit } from '@angular/core';
import { moveIn, fallIn } from "../../resources/router.animation";
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [moveIn(), fallIn()],
  host: { '[@moveIn]': '' }
})
export class LoginComponent implements OnInit {
  error: any;

  constructor(public af: AuthenticationService) {
    
  }

  ngOnInit() {
  }

  loginFb() {
    this.af.SignInFacebook()
  }

  loginGoogle() {
    this.af.SignInGoogle()
  }
}
