import { SignupComponent } from './signup/signup.component';
import { MembersComponent } from './members/members.component';
import { LoginComponent } from './login/login.component';
import { EmailComponent } from './email/email.component';

export const pages = [
    SignupComponent,
    MembersComponent,
    LoginComponent,
    EmailComponent
]
export * from './signup/signup.component';
export * from './members/members.component';
export * from './login/login.component';
export * from './email/email.component';