import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import * as fromPages from "./pages";


const routes: Routes = [
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full"
  },
  {
    path: "login",
    component: fromPages.LoginComponent
  },
  {
    path: "members",
    component: fromPages.MembersComponent
  },
  {
    path: "login-email",
    component: fromPages.EmailComponent
  },
  {
    path: "signup",
    component: fromPages.SignupComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
