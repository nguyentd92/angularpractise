import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <nb-layout>
      <nb-layout-column>
      <div class="app-container">
        <router-outlet></router-outlet>
        </div>
      </nb-layout-column>
    </nb-layout>
  `,
  styles: [
    `.app-container {
      box-sizing: border-box;
    }`,
    `nb-layout-column {
      padding: 0 !important;
    }`
  ]
})
export class AppComponent {
  title = 'AngularFirebaseDN';
}
