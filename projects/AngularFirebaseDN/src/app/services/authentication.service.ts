import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { Observable, BehaviorSubject } from 'rxjs';
import * as firebase from 'firebase/app';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  userDataSubject = new BehaviorSubject<firebase.User>(null);
  userData$: Observable<firebase.User>;

  constructor(private angularFireAuth: AngularFireAuth, private router: Router) {
    this.userData$ = this.userDataSubject.asObservable();
    angularFireAuth.authState.pipe(tap(e => {
      if (!e) { this.router.navigateByUrl("/login") } else {
        this.router.navigateByUrl("/members")
      }
    })).subscribe(u => this.userDataSubject.next(u));
  }

  /* Sign up */
  SignUp(email: string, password: string) {
    this.angularFireAuth
      .auth
      .createUserWithEmailAndPassword(email, password)
      .then(res => {
        console.log('Successfully signed up!', res);
      })
      .catch(error => {
        console.log('Something is wrong:', error.message);
      });
  }

  /* Sign in */
  SignIn(email: string, password: string) {
    this.angularFireAuth
      .auth
      .signInWithEmailAndPassword(email, password)
      .then(res => {
        console.log('Successfully signed in!');
      })
      .catch(err => {
        console.log('Something is wrong:', err.message);
      });
  }

  SignInFacebook() {
    this.angularFireAuth.auth.signInWithPopup(
      new firebase.auth.FacebookAuthProvider()
    )
  }

  SignInGoogle() {
    this.angularFireAuth.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    )
  }


  /* Sign out */
  SignOut() {
    this.angularFireAuth
      .auth
      .signOut();
  }

}