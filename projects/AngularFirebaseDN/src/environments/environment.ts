// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAm8wnqyJg68K8dWl-D1UO_sISYgucbvSs",
    authDomain: "angularfirebasedn.firebaseapp.com",
    databaseURL: "https://angularfirebasedn.firebaseio.com",
    projectId: "angularfirebasedn",
    storageBucket: "angularfirebasedn.appspot.com",
    messagingSenderId: "543591790142",
    appId: "1:543591790142:web:baaf2c9a4fcd9fe5a2436f",
    measurementId: "G-HZ5DQRH629"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
